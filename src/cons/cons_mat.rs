pub use super::super::mat;

pub fn eye(n: usize) -> mat::Matrix<f64> {
    let mut res = mat::Matrix::new(n, n);

    for i in 0..n {
        res[(i as isize, i as isize)] = 1.0;
    }

    res
}
