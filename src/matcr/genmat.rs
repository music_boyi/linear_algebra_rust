#[macro_export]
macro_rules! matrix {
    [$($($x:expr),* $(,)?);* $(;)?] => {
        {
            let mut data = vec![];
            $(
                let mut row = vec![];
                $(
                    let t: &dyn mat::IIorF = &$x;
                    row.push(t.to_f64());
                )*
                data.push(row);
            )*
            mat::Matrix::from_vec(data)
        }
    };
}
pub use crate::matrix;
