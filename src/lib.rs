pub mod cons;
pub mod mat;
pub mod matcr;

pub fn print_vector<T>(value: &[T])
where
    T: AsRef<[i32]>,
{
    for slice in value {
        println!("{:?}", slice.as_ref())
    }
}

pub fn add(left: u64, right: u64) -> u64 {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);

        assert_eq!(result, 4);
    }

    #[test]
    fn tes_vec() {
        let mut a = mat::Matrix::from(&[[1, 2, 3]]);
        a[(0, 0)] = 3.0;
        println!("{:?}", a[0])
    }

    #[test]
    fn test_new_mat() {
        let a = mat::Matrix::new(2, 2);
        println!("{:?}", a[0]);
    }

    #[test]
    fn test_add_mat() {
        let a = mat::Matrix::from(&[[1, 2, 3]]);
        let b = mat::Matrix::from(&[[4, 5, 6]]);
        let c = (&a + &b).expect("size mismatch!");
        println!("{:?},{:?}, {:?}", c[0], a[0], b[0]);
    }

    #[test]
    fn mac_gen_mat() {
        let a = matcr::genmat::matrix![1, 2, 3,4; 4.5, 5, 6];
        println!("{:?}", a[0]);
    }

    #[test]
    fn borrow() {
        let a = 3;
        let b = 4;
        let c = a + b;
        println!("a = {}, b = {}, c = {}", a, b, c);
    }

    #[test]
    fn test_mul_mat() {
        let a = matcr::genmat::matrix![1, 2, 3;4, 5, 6];
        // let b = matcr::genmat::matrix![1, 4; 2, 5; 3, 6];
        // println!("{:?}, {:?}", a.shape(), b.shape());
        // let c = (&a * &b).expect("size mismatch");
        let c = a * 3;

        c.show();
    }

    #[test]
    fn test_determ() {
        let a = matcr::genmat::matrix![1, 2, 3;7, 5, 6;7, 8, 9];
        let det = a.det();
        println!("{:?}", det);
    }

    #[test]
    fn test_matrix_joint() {
        let a = matcr::genmat::matrix![1, 2, 3; 7, 5, 6; 7 ,8, 9];
        let b = a.adjoint();
        b.show();
    }

    #[test]
    fn test_matrix_pow() {
        let a = matcr::genmat::matrix![2, 5; 4, 3];
        let b = (&a ^ 2).unwrap();
        b.show();
    }
}
